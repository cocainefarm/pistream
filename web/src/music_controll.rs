use yew::prelude::*;

pub struct MusicControll {
    link: ComponentLink<Self>,
}

impl Component for MusicControll {
    type Message = ();
    type Properties = ();

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        MusicControll {
            link
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        true
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        true
    }

    fn view(&self) -> Html {
        html! {
            <div class="columns">
                <div class="column" style="margin-bottom: -0.5rem">
                    <div class="column columns is-half is-mobile px-4 py-2">
                        <div class="column is-one-fifth">
                            <i id="start_stop" class="fas fa-play has-text-primary mr-2"></i>
                            <i class="fas fa-forward has-text-primary"></i>
                        </div>
                    </div>
                </div>
            </div>
        }
    }
}
