use yew::prelude::*;

pub struct StreamControll {
    link: ComponentLink<Self>,
}

impl Component for StreamControll {
    type Message = ();
    type Properties = ();

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        StreamControll {
            link
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        true
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        true
    }

    fn view(&self) -> Html {
        html! {
            <div class="columns is-centered">
                <div class="column is-half has-text-centered is-mobile px-4 py-2">
                    <button id="pi_stream" class="button">{"Start Pi Stream"}</button>
                    <button id="live_stream" class="button">{"Go Live"}</button>
                </div>
            </div>
        }
    }
}
