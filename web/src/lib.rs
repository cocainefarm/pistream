#![recursion_limit="512"]
use wasm_bindgen::prelude::*;
use yew::prelude::*;
use http::Uri;
use http::uri::Scheme;
use anyhow::Error;
use yew::services::fetch::{FetchService, FetchTask, Request, Response};
use yew::services::websocket::{WebSocketService, WebSocketStatus, WebSocketTask};
use yew::format::{Json, Nothing};
use serde::{Deserialize, Serialize};
use wasm_bindgen::prelude::*;
use chrono::{DateTime, Timelike, Utc, Local, TimeZone};

mod events;
mod music_controll;

use crate::events::Events;
use crate::music_controll::MusicControll;

type AsBinary = bool;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Message {
    pub id: Option<String>,
    pub _id: Option<String>,
    pub name: String,
    pub formatted_amount: Option<String>,
    pub message: Option<String>,
    pub months: Option<u16>,
    pub sub_plan: Option<String>,
    pub sub_type: Option<String>,
    pub gifter: Option<String>,
    pub created_at: String,
    pub viewers: Option<u16>,
    #[serde( alias = "type" )]
    pub t: String,
    pub amount: Option<String>,
    pub raiders: Option<u16>,
    pub priority: Option<u16>
}

struct Model {
    link: ComponentLink<Self>,
    fetch_task: Option<FetchTask>,
    host: Uri,
    ws: WebSocketTask,
    events: Vec<Message>,
    temp: String,
    bitrate: String,
    online_status: bool,
    online: bool,
    wait_online: bool,
    live: bool,
    music: bool
}

pub enum WsAction {
    Connect,
    SendData(AsBinary),
    Disconnect,
    Lost,
}

pub enum Msg {
    PiStream,
    Obs,
    Music,
    MusicSkip,
    WsAction(WsAction),
    WsReady(Result<String, Error>),
    ReceiveResponse(Result<Resp, Error>),
}

impl From<WsAction> for Msg {
    fn from(action: WsAction) -> Self {
        Msg::WsAction(action)
    }
}

#[derive(Deserialize, Debug)]
pub struct Resp {
    #[serde( alias = "type" )]
    t: String,
    data: Vec<Message>
}

#[derive(Deserialize, Debug)]
pub struct WsResp {
    #[serde( alias = "type" )]
    t: String,
    msg: String
}

impl Component for Model {
    type Message = Msg;
    type Properties = ();
    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        let host = yew::utils::host().unwrap_or("".to_string());
        let host: Uri = host.parse().unwrap();
        let callback = link.callback(|data| Msg::WsReady(data));
        let notification = link.batch_callback(|status| match status {
            WebSocketStatus::Opened => vec![],
            WebSocketStatus::Closed | WebSocketStatus::Error => {
                vec![WsAction::Lost.into()]
            }
        });
         //const ws_uri = "wss://" + location.hostname + ":2794"

        let p = match host.scheme() {
            Some(s) => match s.as_str() {
                "http" => "ws",
                "https" => "wss",
                _ => "ws"
            },
            None => "ws"

        };
        let wss_uri = format!("{}://{}:2794", p, host.host().unwrap());
        let task = WebSocketService::connect_text(&wss_uri, callback, notification).unwrap();
        let temp = "0.0 °C".to_string();
        let bitrate = "0 kbit/s".to_string();

        let req = Request::get("/last_event").body(Nothing).unwrap();
        let callback = link.callback(|response: Response<Json<Result<Resp, Error>>>| {
            let Json(data) = response.into_body();
            Msg::ReceiveResponse(data)
        });
        let t = FetchService::fetch(req, callback).expect("failed to start request");
        let fetch_task = Some(t);

        //TODO check api if allready online and live
        let online = false;
        let online_status = false;
        let live = false;
        let wait_online = false;
        let music = false;
        Self {
            link,
            fetch_task,
            host,
            ws: task,
            events: Vec::new(),
            temp,
            bitrate,
            online,
            online_status,
            wait_online,
            live,
            music
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::WsReady(response) => {
                log::info!("{:?}", response);
                if let Ok(r) = response {
                    let r = serde_json::from_str::<WsResp>(&r);
                    match r {
                        Ok(r) => {
                            match r.t.as_str() {
                                "temp" => {
                                    self.temp = r.msg;
                                },
                                "online" => {
                                    self.wait_online = false;
                                    self.online_status = r.msg.parse().unwrap();
                                },
                                "bitrate" => {
                                    let bit: f32 = r.msg.parse().unwrap();
                                    if bit > 1000.0 {
                                        self.bitrate = format!("{:.2} mbit/s", bit / 1000.0);
                                    } else {
                                        self.bitrate = format!("{:.0} kbit/s", bit);
                                    }
                                },
                                "event" => {
                                    let event = serde_json::from_str::<Message>(&r.msg);
                                    match event {
                                        Ok(e) => {
                                            self.events.remove(0);
                                            self.events.push(e);
                                        },
                                        Err(e) => log::error!("{:?}", e)
                                    }
                                }
                                _ => {}
                            }
                        },
                        Err(e) => log::error!("{:?}", e)
                    }
                }
            },
            Msg::PiStream => {
                self.online = !self.online;
                let mode = if self.online {
                    self.wait_online = true;
                    "start"
                } else {
                    "stop"
                };
                let req = Request::post(format!("/stream/{}", mode)).body(Nothing).unwrap();
                let callback = self.link.callback(|response: Response<Json<Result<Resp, Error>>>| {
                    let Json(data) = response.into_body();
                    Msg::ReceiveResponse(data)
                });
                let task = FetchService::fetch(req, callback).expect("failed to start request");
                self.fetch_task = Some(task);
            },
            Msg::Obs => {
                self.live = !self.live;
                let mode = if self.live {
                    "start"
                } else {
                    "stop"
                };
                let req = Request::post(format!("/obs/{}", mode)).body(Nothing).unwrap();
                let callback = self.link.callback(|response: Response<Json<Result<Resp, Error>>>| {
                    let Json(data) = response.into_body();
                    Msg::ReceiveResponse(data)
                });
                let task = FetchService::fetch(req, callback).expect("failed to start request");
                self.fetch_task = Some(task);
            },
            Msg::Music => {
                self.music = !self.music;
                let mode = if self.music {
                    "start"
                } else {
                    "stop"
                };
                let req = Request::post(format!("/music/{}", mode)).body(Nothing).unwrap();
                let callback = self.link.callback(|response: Response<Json<Result<Resp, Error>>>| {
                    let Json(data) = response.into_body();
                    Msg::ReceiveResponse(data)
                });
                let task = FetchService::fetch(req, callback).expect("failed to start request");
                self.fetch_task = Some(task);
            },
            Msg::MusicSkip => {
                let req = Request::post("/music/skip").body(Nothing).unwrap();
                let callback = self.link.callback(|response: Response<Json<Result<Resp, Error>>>| {
                    let Json(data) = response.into_body();
                    Msg::ReceiveResponse(data)
                });
                let task = FetchService::fetch(req, callback).expect("failed to start request");
                self.fetch_task = Some(task);
            },
            Msg::ReceiveResponse(response) => {
                log::info!("{:?}", response);
                if let Ok(resp) = response {
                    match resp.t.as_str() {
                        "events" => {
                            //let events: Vec<Message> = serde_json::from_str(&resp.data).unwrap();
                            self.events = resp.data;
                        }
                        _ => {}
                    }
                }
            },
            _ => {}
        }
        true
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        // Should only return "true" if new properties are different to
        // previously received properties.
        // This component has no properties so we will always return "false".
        false
    }

    fn view(&self) -> Html {
        html! {
            <div>
                //<Modal />
                //<Msg />
                <div class="container mx-4">
                    //NOTE info box
                    <div class="columns is-centered mt-2">
                        <div class="column is-half is-mobile card px-4 py-2">
                            <div class="columns is-mobile">
                                <div class="column has-text-centered">{ &self.temp }</div>
                                <div class="column is-one-fifth has-text-centered">
                                    { self.is_live() }
                                </div>
                                <div class="column is-one-fifth has-text-centered">
                                    { self.is_online() }
                                </div>
                                <div class="column has-text-centered">{ &self.bitrate }</div>
                            </div>
                        </div>
                    </div>
                    <div class="columns is-centered mt-2">
                        <div class="column is-half is-mobile card px-4 py-2">
                                { if self.events.is_empty() {
                                    html! {
                                        <div class="p-2">
                                            { "Noting here" }
                                        </div>
                                    }
                                } else {
                                    html! {}
                                }}
                            {self.get_events()}
                        </div>
                    </div>
                    <div class="columns is-centered pt-2">
                        <div class="card has-text-primary is-size-5 py-2 px-4">
                            { self.is_music_playing() }
                            <i onclick=self.link.callback(|_| Msg::MusicSkip) class="nf nf-mdi-skip_next"></i>
                        </div>
                    </div>
                    <div class="columns is-centered">
                        <div class="column is-half has-text-centered is-mobile px-4 py-2">
                            <button disabled=self.wait_online
                                     onclick=self.link.callback(|_| Msg::PiStream) class="button mr-2">
                                { self.is_online_button() }
                            </button>
                            <button onclick=self.link.callback(|_| Msg::Obs) class="button">
                                { self.is_live_button() }
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        }
    }
}

impl Model {
    fn is_online_button(&self) -> &str {
        if self.wait_online {
            return "Waiting to start..."
        }
        if self.online {
            "Stop Pi Stream"
        } else {
            "Start Pi Stream"
        }
    }

    fn is_online(&self) -> Html {
        let class = format!("is-size-5 nf {}",
            if self.online_status {
                "has-text-success nf-mdi-signal"
            } else {
                "has-text-danger nf-mdi-signal"
            }
        );
        html! { <i class=class></i> }
    }

    fn is_live_button(&self) -> &str {
        if self.live {
            "Go Offline"
        } else {
            "Go Live"
        }
    }

    fn is_live(&self) -> Html {
        let class = format!(
            "is-size-5 nf nf-oct-broadcast {}",
            if self.live {
                "has-text-success"
            } else {
                "has-text-danger"
            }
        );
        html! { <i class=class></i> }
    }

    fn get_events(&self) -> Html {
        html! {
            {
                for self.events.iter().map(|event| {
                    let msg = match event.t.as_str() {
                        "follow" => "has followed".to_string(),
                        "subscription" => {
                            let mut out = "".to_string();
                            if let Some(month) = event.months {
                                if month > 1 {
                                    out = format!("has subscribed for {} month", month);
                                }
                            }
                            if let Some(user) = &event.gifter {
                                out = format!("got a free sub from {}", user);
                            }
                            if out != "" {
                                out
                            } else {
                                "has subscribed".to_string()
                            }
                        },
                        "resub" => format!("has resubt for {} month", event.months.clone().unwrap()),
                        "bits" => format!("has dropt {} bits", event.amount.clone().unwrap()),
                        _ => "".to_string(),
                    };
                    let time = DateTime::parse_from_rfc3339(&event.created_at).unwrap();
                    let out = format!(
                        "{:02}:{:02} | {} {}",
                        time.hour(),
                        time.minute(),
                        event.name,
                        msg
                    );
                    html! { <div class="border is-clipped p-2 event">{ out }</div> }
                })
            }
        }
    }

    fn is_music_playing(&self) -> Html {
        let class = format!("nf pr-4 {}" ,
        if self.music {
            "nf-mdi-pause"
        } else {
            "nf-mdi-play"
        });
        html! { <i onclick=self.link.callback(|_| Msg::Music) class=class></i> }
    }
}

#[wasm_bindgen(start)]
pub fn run_app() {
    wasm_logger::init(wasm_logger::Config::default());


    App::<Model>::new().mount_to_body();
}
