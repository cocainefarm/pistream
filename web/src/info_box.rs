use yew::prelude::*;

use crate::Msg;

pub struct InfoBox {
    link: ComponentLink<Self>,
}

impl Component for InfoBox {
    type Message = Msg;
    type Properties = ();

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        InfoBox {
            link
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        true
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        true
    }

    fn view(&self) -> Html {
        html! {
        }
    }
}
