use std::sync::{Arc, Mutex, mpsc::Sender};
use std::{thread, time};
use std::fs;
use std::process::Command;

use crate::nightbot::api;
use crate::api::ws::WsUpdate;
use crate::player::player::Message;

pub struct Downloader {
    token: String,
    finished: Vec<String>
}

impl Downloader {
    pub fn new(token: String) -> Downloader {
        Downloader {
            token: token.clone(),
            finished: Vec::new()
        }
    }

    pub async fn start(
        &mut self,
        sender: Arc<Mutex<Sender<Message>>>,
        ws_sender: Arc<Mutex<Sender<WsUpdate>>>
    ) {
        loop {
            if let Ok(queue) = api::get_queue(&self.token).await {
                let list: Vec<String> = fs::read_dir("/home/d3fus/music/playlist/").unwrap().map(|x| {
                    let x = x.unwrap().path();
                    let x = x.to_str().unwrap().split("_").collect::<Vec<&str>>()[1]
                        .split(".mp3").collect::<Vec<&str>>()[0];
                    x.to_string()
                }).collect();
                for q in queue.queue {
                    if !self.finished.contains(&q._id) && !list.contains(&q._id) {
                        let s = ws_sender.lock().unwrap();
                        let _ = s.send(WsUpdate::DownloadStart(q.track.title.clone()));
                        drop(s);
                        let i = self.finished.len();
                        let filename = format!("{}_{}", i, q._id);
                        let _ = Command::new("youtube-dl")
                            .arg("-x")
                            .arg("--audio-format")
                            .arg("mp3")
                            .arg("--output")
                            .arg(format!("/tmp/{}.%(ext)s", filename))
                            .arg(&q.track.url)
                            .output()
                            .unwrap();
                        fs::copy(
                            format!("/tmp/{}.mp3", filename),
                            format!("/home/d3fus/music/playlist/{}.mp3", filename)
                        ).unwrap();
                        fs::remove_file(format!("/tmp/{}.mp3", filename)).unwrap();
                        let s = ws_sender.lock().unwrap();
                        let _ = s.send(WsUpdate::DownloadFin(q.track.title));
                        drop(s);
                        let s = sender.lock().unwrap();
                        let _ = s.send(Message::AddSong(format!("/home/d3fus/music/playlist/{}.mp3", filename)));
                        drop(s);
                        self.finished.push(q._id);
                    } else {
                        let filename = fs::read_dir("/home/d3fus/music/playlist/").unwrap();
                        for f in filename {
                            let x = f.unwrap();
                            let x = x.path();
                            let x = x.to_str().unwrap();
                            if x.contains(&q._id) {
                                let s = sender.lock().unwrap();
                                let _ = s.send(Message::AddSong(x.to_string()));
                                drop(s);
                            }
                        }
                    }
                }
            }
            let min = time::Duration::from_secs(60);
            thread::sleep(min);
        }
    }
}
