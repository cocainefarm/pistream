use reqwest;

use crate::models::{
    config::Config,
    nightbot::{SongRequestQueue, NightBotResp}
};

pub async fn get_tokens(config: &mut Config) -> String {
    let uri = format!("https://api.nightbot.tv/oauth2/token");
    let data = [
        ("client_id", config.nightbot_client_id.clone()),
        ("client_secret", config.nightbot_client_secret.clone()),
        ("grant_type", "refresh_token".to_string()),
        ("redirect_uri", "https://test.com".to_string()),
        ("refresh_token", config.nightbot_refresh_token.clone())
    ];
    let client = reqwest::Client::new();
    let req = client.post(&uri).form(&data).send().await.unwrap();
    let json: NightBotResp = req.json().await.unwrap();
    config.nightbot_refresh_token = json.refresh_token.clone();
    println!("{:?}", config);
    config.save();
    json.access_token
}

pub async fn del_item(token: &str, id: &str) {
    let client = reqwest::Client::new();
    let uri = format!("https://api.nightbot.tv/1/song_requests/queue/{}", &id);
    let _ = client.delete(&uri).header("authorization", format!("Bearer {}", &token)).send().await.unwrap();
}

pub async fn get_queue(token: &str) -> Result<SongRequestQueue, reqwest::Error> {
    let client = reqwest::Client::new();
    let uri = "https://api.nightbot.tv/1/song_requests/queue";
    let req = client.get(uri).header("authorization", format!("Bearer {}", &token)).send().await?;
    Ok(req.json().await.unwrap())
}
