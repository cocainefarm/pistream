use websocket::client::ClientBuilder;
use websocket::message::Message;
use std::{thread, time};
use std::time::SystemTime;
use serde::Deserialize;
use websocket::OwnedMessage;
use std::sync::mpsc;

#[derive(Deserialize, Debug)]
struct Pubsub {
    #[serde(alias = "type")]
    t: String,
    nonce: String,
    data: PubsubData
}

#[derive(Deserialize, Debug)]
struct PubsubData {

}

#[derive(Deserialize, Debug)]
struct PubsubTest {
    #[serde(alias = "type")]
    t: String,
    data: PubsubTopic
}

#[derive(Deserialize, Debug)]
struct PubsubTopic {
    topic: String,
    message: String
}

#[derive(Deserialize, Debug)]
struct PubsubResponse<T> {
    #[serde(alias = "type")]
    t: String,
    data: T
}

#[derive(Deserialize, Debug)]
struct PubsubReward {
    timestamp: String,
    redemption: PubsubRedemtion
}

#[derive(Deserialize, Debug)]
pub struct PubsubRedemtion {
    pub id: String,
    pub reward: Reward,
    pub user_input: Option<String>,
    pub status: String
}

#[derive(Deserialize, Debug)]
pub struct Reward {
    pub id: String,
    pub title: String,
}

pub fn start_pubsub(sender: mpsc::Sender<PubsubRedemtion>) {

    let mut client = ClientBuilder::new("wss://pubsub-edge.twitch.tv")
        .unwrap()
        .connect_secure(None)
        .unwrap();
    client.set_nonblocking(true).unwrap();


    let ping = Message::text("{\"type\": \"PING\"}");
    let lisen = Message::text("{\"type\": \"LISTEN\",\"nonce\": \"44h1k13746815ab1r2\",\"data\":{\"topics\": [\"channel-points-channel-v1.91969580\"], \"auth_token\":\"ifirlsuremlgi8wfj1swb4zr1uj029\"}}");

    if let Err(e) = client.send_message(&lisen){
        println!("{:?}", e);
    };

    let mut timer = SystemTime::now();
    loop {
        match timer.elapsed() {
            Ok(e) => {
                if e.as_secs() >= 3 * 60 {
                    if let Err(e) = client.send_message(&ping) {
                        println!("{:?}", e);
                    };
                    timer = SystemTime::now();
                    if let Ok(OwnedMessage::Text(pong)) = client.recv_message() {
                        if !pong.contains("pong") {
                            break
                        }
                    }
                }
            },
            Err(_) => {}
        }
        if let Ok(OwnedMessage::Text(msg)) = client.recv_message() {
            println!("{:?}", msg);
            if msg.contains("reward-redeemed") {
                let msg = serde_json::from_str::<PubsubTest>(&msg).unwrap();
                let msg = serde_json::from_str::<PubsubResponse<PubsubReward>>(&msg.data.message).unwrap();
                let _ = sender.send(msg.data.redemption);
            }
        }
        let five_sec = time::Duration::from_secs(5);
        thread::sleep(five_sec);
    }

}
