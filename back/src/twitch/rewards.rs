use reqwest::header;
use std::collections::HashMap;
use std::sync::{Arc, Mutex, mpsc::Receiver};
use std::fs;
use std::fmt;
use std::fs::File;
use std::io::Write;

use crate::models::{config::Config, reward::{Reward, RefreshToken, ConfigReward, TwitchResp}};

pub enum RewardsMsg {
    NewToken(String),
    Uri(String)
}

pub async fn start_reards(
    rewards: Arc<Mutex<Rewards>>,
    mut token: String,
    mut config: Config,
    resiver: Receiver<RewardsMsg>
) {
    if token == "" {
        let mut uri = "".to_string();
        for msg in &resiver {
            match msg {
                RewardsMsg::NewToken(m) => token = m,
                RewardsMsg::Uri(u) => uri = u
            };
            break;
        }
        match resiver.recv().unwrap() {
            RewardsMsg::NewToken(m) => token = m,
            RewardsMsg::Uri(u) => uri = u
        };
        let json = get_token(token, uri, &config).await;
        config.twitch_refresh_token = json.refresh_token;
        config.save();
        token = json.access_token;
        let mut re = rewards.lock().unwrap();
        re.update_header(token, config);
        drop(re);
    }
    //NOTE load local config
    //let reward = Rewards::new(token, config);
    let base = xdg::BaseDirectories::with_prefix("pistream").unwrap();
    let path = base.place_config_file("rewards.json").unwrap();
    let config_str = match fs::read_to_string(&path) {
        Ok(s) => s,
        Err(_) => "[]".to_string()
    };
    let local_configs = match serde_json::from_str::<Vec<ConfigReward>>(&config_str) {
        Ok(j) => j,
        Err(_) => panic!("config file empty or brocken")
    };
    let local_files = fs::read_dir("/home/d3fus/music/soundboard").unwrap();

    let mut new_local_config = Vec::new();
    for sound in local_files {
        if let Ok(sound) = sound {
            let pos = local_configs.iter().rposition(|x| {
                let name = &x.name == &sound.file_name().to_str().unwrap();
                let path = sound.path().to_str().unwrap() == x.sound_path;
                return name || path
            });
            match pos {
                Some(i) => {
                    new_local_config.push(local_configs[i].clone());
                },
                None => {
                    let name = sound.file_name();
                    let name = name.to_str().unwrap();
                    let new_config = if name.contains("tts") {
                        ConfigReward::new_tts()
                    } else {
                        let path = sound.path();
                        let path = path.to_str().unwrap();
                        ConfigReward::new(name.to_string(), path.to_string())
                    };
                    new_local_config.push(new_config);
                }
            }
        }
    }

    let re = rewards.lock().unwrap();
    let twitch_rewards = re.get_rewards().await.unwrap().data;
    drop(re);

    //push all her and del old
    //let mut new_rewards = twitch_rewards.clone();
    for (p, r) in twitch_rewards.clone().iter().enumerate() {
        let pos = new_local_config.iter().rposition(|x| {
            match &x.twitch_reward {
                Some(re) => {
                    re.id == r.id
                },
                None => {
                    r.title.contains(&x.name)
                }
            }
        });
        match pos {
            Some(i) => {
                //new_rewards.push(twitch_rewards.remove(p));
                let cost = new_local_config[i].cost != r.cost;
                let enabled = new_local_config[i].is_enabled != r.is_enabled;
                new_local_config[i].twitch_reward = if cost || enabled {
                    let mut tmp = new_local_config[i].clone();
                    tmp.twitch_reward = Some(r.clone());
                    let re = rewards.lock().unwrap();
                    let new_r = re.update_reward(&tmp).unwrap();
                    drop(re);
                    Some(new_r.data[0].clone())
                } else {
                    Some(r.clone())
                };
            },
            None => {
                let re = rewards.lock().unwrap();
                re.del_reward(r.clone()).await;
                drop(re);
            }
        }
    }

    for (i, r) in new_local_config.clone().iter().enumerate() {
        if let None = r.twitch_reward {
            let re = rewards.lock().unwrap();
            let new_r = re.create_reward(&r).await.unwrap();
            drop(re);
            new_local_config[i].twitch_reward = Some(new_r.data[0].clone());
        }
    }

    let mut file = File::create(&path).unwrap();
    let config = serde_json::to_string_pretty(&new_local_config).unwrap();
    let _ = file.write_all(config.as_bytes()).unwrap();

    //thread::spawn(|| );
}

pub enum Error {
    InvalidToken
}

impl fmt::Debug for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_tuple("")
         .field(&"invalid token")
            .finish()
    }
}

impl From<reqwest::Error> for Error {
    fn from(error: reqwest::Error) -> Self {
        Error::InvalidToken
    }
}

type Resp<T> = Result<TwitchResp<T>, Error>;
type R<T> = Result<T, Error>;

pub struct Rewards {
    client: reqwest::Client,
    headers: header::HeaderMap,
    uri: String,
    token: String,
    channel_id: String
}

impl Rewards {
    pub fn new(token: String, config: Config) -> Rewards {
        let mut headers = header::HeaderMap::new();
        headers.append("Client-Id", config.twitch_cliend_id.clone().parse().unwrap());
        headers.append("Authorization", format!("Bearer {}", token).parse().unwrap());
        let client = reqwest::Client::new();
        Rewards {
            client,
            headers,
            uri: "https://api.twitch.tv/helix/channel_points/custom_rewards".to_string(),
            token,
            channel_id: config.twitch_profile_id
        }
    }
    pub fn update_header(&mut self, token: String, config: Config) {
        let mut headers = header::HeaderMap::new();
        headers.append("Client-Id", config.twitch_cliend_id.clone().parse().unwrap());
        headers.append("Authorization", format!("Bearer {}", token).parse().unwrap());
        self.headers = headers;
    }

    pub async fn get_rewards(&self) -> Resp<Reward> {
        let uri = format!(
            "{}?broadcaster_id={}",
            self.uri,
            self.channel_id
        );
        let resp = self.client.get(&uri).headers(self.headers.clone()).send().await?;
        Ok(resp.json().await?)
    }

    pub async fn del_reward(&self, reward: Reward) {
        let uri = format!(
            "{}?broadcaster_id={}&id={}",
            self.uri,
            self.channel_id,
            reward.id
        );
        let _ = self.client.delete(&uri).send().await;
    }

    pub async fn create_reward(&self, conf: &ConfigReward) -> Resp<Reward> {
        let uri = format!(
            "{}?broadcaster_id={}",
            self.uri,
            self.channel_id
        );
        let mut data = HashMap::new();
        data.insert("title", format!("SR: {}", conf.name));
        data.insert("cost", conf.cost.to_string());
        data.insert("is_enabled", conf.is_enabled.to_string());
        let resp = self.client
                       .post(&uri)
                       .headers(self.headers.clone())
                       .json(&data).send().await?;
        Ok(resp.json().await?)
    }

    pub fn update_reward(&self, conf: &ConfigReward) -> Resp<Reward> {
        println!("{:?}", conf);
        let id = conf.twitch_reward.clone().unwrap().id;
        let uri = format!(
            "{}?broadcaster_id={}&id={}",
            self.uri,
            self.channel_id,
            id
        );
        let mut data = HashMap::new();
        data.insert("title", format!("SR: {}", conf.name));
        data.insert("cost", conf.cost.to_string());
        data.insert("is_enabled", conf.is_enabled.to_string());
        let client = reqwest::blocking::Client::new();
        let resp = client
                       .patch(&uri)
                       .headers(self.headers.clone())
                       .json(&data).send()?;
        Ok(resp.json()?)
    }
}

pub async fn get_token(code: String, return_uri: String, config: &Config) -> RefreshToken {
    let uri = "https://id.twitch.tv/oauth2/token";
    let uri = format!(
        "{}?client_id={}&client_secret={}&code={}&grant_type=authorization_code&redirect_uri={}",
        uri,
        config.twitch_cliend_id,
        config.twitch_cliend_secret,
        code,
        return_uri
    );
    let client = reqwest::Client::new();
    let req = client.post(&uri).send().await.unwrap();
    let json = req.json::<RefreshToken>().await.unwrap();
    json
}

pub async fn renew_token(config: &mut Config) -> R<String> {
    let parms = [
        ("grant_type", "refresh_token"),
        ("refresh_token", &config.twitch_refresh_token),
        ("client_id", &config.twitch_cliend_id),
        ("client_secret", &config.twitch_cliend_secret)
    ];
    let client = reqwest::Client::new();
    let req = client.post("https://id.twitch.tv/oauth2/token")
        .form(&parms)
        .send()
        .await
        .unwrap();
    let json = req.json::<RefreshToken>().await?;
    config.twitch_refresh_token = json.refresh_token;
    config.save();
    Ok(json.access_token)
}
