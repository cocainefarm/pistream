use std::thread;
use serde::Serialize;
use websocket::sync::Server;
use websocket::Message;
use websocket::server::upgrade::WsUpgrade;
use std::sync::mpsc::{Receiver, Sender, channel};
use std::fs::File;
use std::io::Read;
use native_tls::{Identity, TlsAcceptor};

#[derive(Clone)]
pub enum WsUpdate {
    TempUpdate(f32),
    Bitrate(f32),
    Online(bool),
    Live(bool),
    DownloadStart(String),
    DownloadFin(String),
    Event(String)
}

struct RecHandler {
    recv: Receiver<WsUpdate>,
    sender: Vec<Sender<WsUpdate>>
}

impl RecHandler {
    pub fn new(recv: Receiver<WsUpdate>) -> (RecHandler, Vec<Receiver<WsUpdate>>) {
        let mut sender = Vec::new();
        let mut resiv = Vec::new();
        for i in 0..=4 {
            let (s, r) = channel();
            sender.push(s);
            resiv.push(r);
        }
        let handler = RecHandler {
            recv,
            sender
        };
        (handler, resiv)
    }

    pub fn start(self) {
        for msg in self.recv {
            for send in &self.sender {
                let _ = send.send(msg.clone());
            }
        }
    }
}

#[derive(Debug, Serialize)]
struct Out {
    #[serde( alias = "type" )]
    t: String,
    msg: String
}

impl Out {
    fn new(t: String, msg: String) -> Out {
        Out {
            t,
            msg
        }
    }
}

pub fn start_server(resiv: Receiver<WsUpdate>) {
    let file = File::open("./private/identity.pfx");
    match file {
        Ok(mut file) => {
            let mut pkcs12 = vec![];
            file.read_to_end(&mut pkcs12).unwrap();
            let pkcs12 = Identity::from_pkcs12(&pkcs12, "1234").unwrap();
            let acceptor = TlsAcceptor::builder(pkcs12).build().unwrap();

            start_secure(resiv, acceptor);
        },
        Err(_) => {
            start_insecure(resiv);
        }
    };


    //let (handler, r) = RecHandler::new(resiv);
    //thread::spawn(|| handler.start());
    //let mut r = r.into_iter();

    //for request in server.filter_map(Result::ok) {
    //    let r2 = r.next();
    //    thread::spawn(|| {
    //        let mut client = request.accept().unwrap();

    //        let mut tmp_online = None;
    //        let ip = client.peer_addr().unwrap();

    //        println!("Connection from {}", ip);
    //        for msg in r2.unwrap() {
    //            let mut skip = false;
    //            let (msg, t) = match msg {
    //                WsUpdate::TempUpdate(t) => (format!("{:.1} °C", t), "temp"),
    //                WsUpdate::Online(b) => {
    //                    if let Some(t) = tmp_online {
    //                        if t == b {
    //                            skip = true;
    //                        }
    //                    }
    //                    tmp_online = Some(b);
    //                    (format!("{}", b), "online")
    //                },
    //                WsUpdate::Live(b) => (format!("{}", b), "live"),
    //                WsUpdate::DownloadStart(name) => {
    //                    let name = format!("downloading: {}", name);
    //                    (name, "download")
    //                },
    //                WsUpdate::DownloadFin(name) => {
    //                    let name = format!("fin: {}", name);
    //                    (name, "download")
    //                },
    //                WsUpdate::Bitrate(bitrate) => (bitrate.to_string(), "bitrate"),
    //                WsUpdate::Event(event) => (event.replace('"', "\""), "event"),
    //                _ => (String::from("err"), "err")
    //            };
    //            let msg = Out::new(t.to_string(), msg);
    //            let msg = serde_json::to_string(&msg).unwrap();
    //            //let msg = format!("{{\"type\": \"{}\", \"msg\": \"{}\" }}", t, msg);
    //            if !skip {
    //                let _ = client.send_message(&Message::text(msg));
    //            }
    //        }
    //    });
    //}
}

fn start_secure(resiv: Receiver<WsUpdate>, acceptor: TlsAcceptor) {
    let server = Server::bind_secure("0.0.0.0:2794", acceptor).unwrap();
    let (handler, r) = RecHandler::new(resiv);
    thread::spawn(|| handler.start());
    let mut r = r.into_iter();

    for request in server.filter_map(Result::ok) {
        let r2 = r.next();
        thread::spawn(move || {
            let mut client = request.accept().unwrap();

            let mut tmp_online = None;
            let ip = client.peer_addr().unwrap();

            println!("Connection from {}", ip);
            for msg in r2.unwrap() {
                let mut skip = false;
                let (msg, t) = get_msg(msg, &mut tmp_online, &mut skip);
                let msg = Out::new(t.to_string(), msg);
                let msg = serde_json::to_string(&msg).unwrap();
                //let msg = format!("{{\"type\": \"{}\", \"msg\": \"{}\" }}", t, msg);
                if !skip {
                    let _ = client.send_message(&Message::text(msg));
                }
            }
        });
    }
}

fn start_insecure(resiv: Receiver<WsUpdate>) {
    let server = Server::bind("0.0.0.0:2794").unwrap();
    let (handler, r) = RecHandler::new(resiv);
    thread::spawn(|| handler.start());
    let mut r = r.into_iter();

    for request in server.filter_map(Result::ok) {
        let r2 = r.next();
        thread::spawn(move || {
            let mut client = request.accept().unwrap();

            let mut tmp_online = None;
            let ip = client.peer_addr().unwrap();

            println!("Connection from {}", ip);
            for msg in r2.unwrap() {
                let mut skip = false;
                let (msg, t) = get_msg(msg, &mut tmp_online, &mut skip);
                let msg = Out::new(t.to_string(), msg);
                let msg = serde_json::to_string(&msg).unwrap();
                //let msg = format!("{{\"type\": \"{}\", \"msg\": \"{}\" }}", t, msg);
                if !skip {
                    let _ = client.send_message(&Message::text(msg));
                }
            }
        });
    }
}

fn get_msg<'a>(msg: WsUpdate, tmp_online: &mut Option<bool>, skip: &mut bool) -> (String, &'a str) {
    match msg {
        WsUpdate::TempUpdate(t) => (format!("{:.1} °C", t), "temp"),
        WsUpdate::Online(b) => {
            if let Some(t) = tmp_online {
                if t == &b {
                    *skip = true;
                }
            }
            *tmp_online = Some(b);
            (format!("{}", b), "online")
        },
        WsUpdate::Live(b) => (format!("{}", b), "live"),
        WsUpdate::DownloadStart(name) => {
            let name = format!("downloading: {}", name);
            (name, "download")
        },
        WsUpdate::DownloadFin(name) => {
            let name = format!("fin: {}", name);
            (name, "download")
        },
        WsUpdate::Bitrate(bitrate) => (bitrate.to_string(), "bitrate"),
        WsUpdate::Event(event) => (event.replace('"', "\""), "event"),
        _ => (String::from("err"), "err")
    }
}
