use rocket::State;
use std::sync::{Arc, Mutex, mpsc::Sender};
use rocket_contrib::json::Json;
use std::fs;
use std::fs::File;
use std::io::Write;

use crate::models::{config::Config, resp::Resp, reward::ConfigReward};
use crate::twitch::rewards;

#[get("/twitch/token/valid")]
pub async fn token_valide(token: State<'_, Arc<Mutex<String>>>) -> Json<Resp<bool>> {
    let token = token.lock().unwrap().clone();
    Json(Resp::new("twitch_token".to_string(), &token != ""))
}

#[post("/twitch/code?<code>&<uri>")]
pub async fn update_token(
    code: String, uri: String, sender: State<'_, Arc<Mutex<Sender<rewards::RewardsMsg>>>>
) -> Json<Resp<bool>> {
    let sender = sender.lock().unwrap();
    let _ = sender.send(rewards::RewardsMsg::NewToken(code));
    let _ = sender.send(rewards::RewardsMsg::Uri(uri));
    drop(sender);
    Json(Resp::new("twitch_token".to_string(), true))
}

#[get("/twitch/rewards")]
pub async fn get_rewards() -> Json<Resp<Vec<ConfigReward>>>{
    let base = xdg::BaseDirectories::with_prefix("pistream").unwrap();
    let path = base.place_config_file("rewards.json").unwrap();
    let config_str = match fs::read_to_string(&path) {
        Ok(s) => s,
        Err(_) => "[]".to_string()
    };
    let local_configs = match serde_json::from_str::<Vec<ConfigReward>>(&config_str) {
        Ok(j) => j,
        Err(_) => panic!("config file empty or brocken")
    };
    Json(Resp::new("twitch_rewards".to_string(), local_configs))
}

#[patch("/twitch/reward?<id>", data = "<reward>")]
pub async fn update_reward(
    rewards: State<'_, Arc<Mutex<rewards::Rewards>>>,
    id: String,
    reward: Json<ConfigReward>
) -> Json<ConfigReward> {
    let r = rewards.lock().unwrap();
    let resp = r.update_reward(&reward).unwrap();
    drop(r);

    let base = xdg::BaseDirectories::with_prefix("pistream").unwrap();
    let path = base.place_config_file("rewards.json").unwrap();
    let config_str = match fs::read_to_string(&path) {
        Ok(s) => s,
        Err(_) => "[]".to_string()
    };
    let mut local_configs = match serde_json::from_str::<Vec<ConfigReward>>(&config_str) {
        Ok(j) => j,
        Err(_) => panic!("config file empty or brocken")
    };

    let pos = local_configs.iter().rposition(|x|reward.name == x.name).unwrap();
    local_configs[pos] = reward.0;
    local_configs[pos].twitch_reward = Some(resp.data[0].clone());
    let resp = local_configs[pos].clone();

    let mut file = File::create(&path).unwrap();
    let config = serde_json::to_string_pretty(&local_configs).unwrap();
    let _ = file.write_all(config.as_bytes()).unwrap();

    Json(resp)
}
