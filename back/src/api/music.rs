use rocket::State;
use std::sync::{Arc, Mutex, mpsc};
use crate::player::player::Message;

#[post("/music/start")]
pub fn start_music(sender: State<Arc<Mutex<mpsc::Sender<Message>>>>) -> String {
    let s = sender.lock().unwrap();
    let out = s.send(Message::Play);
    println!("{:?}", out);
    drop(s);
    String::from("start")
}

#[post("/music/stop")]
pub fn stop_music(sender: State<Arc<Mutex<mpsc::Sender<Message>>>>) -> String {
    let s = sender.lock().unwrap();
    let out = s.send(Message::Stop);
    println!("{:?}", out);
    drop(s);
    String::from("stop")
}

#[post("/music/skip")]
pub fn skip_music(sender: State<Arc<Mutex<mpsc::Sender<Message>>>>) -> String {
    let s = sender.lock().unwrap();
    let out = s.send(Message::Skip);
    println!("{:?}", out);
    drop(s);
    String::from("skip")
}
