use rocket::State;
use rocket_contrib::json::Json;
use std::sync::{Arc, Mutex};

use crate::models::{
    resp::Resp,
    config::Config,
    models::Streamlabs,
    models::Message
};

//#[get("/status")]
//pub async fn get_status() -> JsonValue {
//
//}

#[get("/config")]
pub async fn get_config(config: State<'_, Config>) -> Json<Resp<Config>> {
    let config = config.inner();
    Json(Resp::new("config".to_string(), config.clone()))
}

#[get("/last_event")]
pub async fn get_last_events(config: State<'_, Config>) -> Json<Resp<Vec<Message>>> {
    let config = config.inner();
    let uri = format!("https://streamlabs.com/api/v5/recentevents/{}", &config.streamlabs_event_id);
    let req = reqwest::get(&uri).await.unwrap();
    let events: Streamlabs = match req.json().await {
        Ok(re) => re,
        Err(_) => return Json(Resp::new_error(vec![]))
    };
    let mut out = events.get_all_messages();
    out.sort();
    let mut out = out.split_off(out.len() -3);
    for item in &mut out {
        item.convert_time();
    }

    Json(Resp::new("events".to_string(), out))
}
