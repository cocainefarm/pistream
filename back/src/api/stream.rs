use rocket::State;
use std::sync::{Arc, Mutex, mpsc::Sender};
use rocket_contrib::json::Json;
use serde::Serialize;

use crate::models::resp::Resp;
use crate::api::ws::WsUpdate;

pub enum Stream {
    Start,
    Stop
}

#[derive(Debug, Serialize)]
pub struct RespStream {
    stream: bool
}

impl RespStream {
    pub fn new(stream: bool) -> RespStream {
        RespStream {
            stream
        }
    }
}

#[post("/stream/start")]
pub async fn start_stream(state: State<'_, Arc<Mutex<Sender<Stream>>>>) -> Json<Resp<RespStream>> {
    let sender = state.inner().lock().unwrap();
    let _ = sender.send(Stream::Start);
    drop(sender);
    let resp = RespStream::new(true);
    let resp = Resp::new_stream(resp);
    Json(resp)
}

#[post("/stream/stop")]
pub async fn stop_stream(state: State<'_, Arc<Mutex<Sender<Stream>>>>) -> Json<Resp<RespStream>> {
    let sender = state.inner().lock().unwrap();
    let _ = sender.send(Stream::Stop);
    drop(sender);
    let resp = RespStream::new(false);
    let resp = Resp::new_stream(resp);
    Json(resp)
}

#[post("/obs/start")]
pub async fn start_obs(state: State<'_, Arc<Mutex<Sender<WsUpdate>>>>) {
    let s = state.inner().lock().unwrap();
    let _ = s.send(WsUpdate::Live(true));
    drop(s);
    let resp = reqwest::blocking::get("http://10.10.0.20:8000/start");
}

#[post("/obs/stop")]
pub async fn stop_obs(state: State<'_, Arc<Mutex<Sender<WsUpdate>>>>) {
    let s = state.inner().lock().unwrap();
    let _ = s.send(WsUpdate::Live(false));
    drop(s);
    let resp = reqwest::blocking::get("http://10.10.0.20:8000/stop");
}
