use rocket::response::content;
use rocket::http::ContentType;

#[get("/")]
pub async fn get_index() -> content::Html<&'static str> {
    let index = include_str!("../../../web/static/index.html");
    content::Html(&index)
}

#[get("/bulma.css")]
pub async fn get_bulma() -> content::Css<&'static str> {
    let bulma = include_str!("../../../web/static/bulma.css");
    content::Css(&bulma)
}

#[get("/nerd-fonts-generated.min.css")]
pub async fn get_nerd_fonts() -> content::Css<&'static str> {
    let nerd_fonts = include_str!("../../../web/static/nerd-fonts-generated.min.css");
    content::Css(&nerd_fonts)
}

#[get("/fonts/NerdFontsSymbols-2048-em Nerd Font Complete.ttf")]
pub async fn get_nerd_fonts_font() -> content::Content<&'static [u8]> {
    let font = include_bytes!("../../../web/static/Symbols-2048-em Nerd Font Complete.ttf");
    content::Content(ContentType::new("application", "font-woff2"), font)
}

#[get("/wasm.js")]
pub async fn get_wasm_js() -> content::JavaScript<&'static str> {
    let js = include_str!("../../../web/static/wasm.js");
    content::JavaScript(&js)
}

#[get("/wasm_bg.wasm")]
pub async fn get_wasm_bg() -> content::Content<&'static [u8]> {
    let binery = include_bytes!("../../../web/static/wasm_bg.wasm");
    content::Content(ContentType::WASM, binery)
}


//#[get("/bulma-slider.min.css")]
//pub async fn get_bulma_slider() -> content::Css<&'static str> {
//    let bulma = include_str!("../css/bulma-slider.min.css");
//    content::Css(&bulma)
//}

//#[get("/all.min.js")]
//pub async fn get_font_awsome() -> content::JavaScript<&'static str> {
//    let awsome = include_str!("../js/all.min.js");
//    content::JavaScript(&awsome)
//}

//#[get("/bulma-slider.min.js")]
//pub async fn get_bulma_slider_js() -> content::JavaScript<&'static str> {
//    let slider = include_str!("../js/bulma-slider.min.js");
//    content::JavaScript(&slider)
//}
