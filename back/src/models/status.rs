pub struct Status {
    live: bool,
    online: bool,
    bitrate: u32,
    playing_music: bool
}

impl Status {
    pub fn new() -> Status {
        Status {
            live: false,
            online: false,
            bitrate: 0,
            playing_music: false
        }
    }
}
