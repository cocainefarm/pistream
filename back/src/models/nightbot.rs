use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Clone)]
pub struct QueueSkip {
    pub status: u32,
    pub item: Song
}

#[allow(non_snake_case)]
#[derive(Debug, Deserialize, Clone)]
pub struct SongRequestQueue {
    pub _total: Option<u32>,
    pub _currentSong: Option<Song>,
    pub _requestsEnabled: Option<bool>,
    pub _searchProvider: Option<String>,
    pub _providers: Option<Vec<String>>,
    pub status: u32,
    pub queue: Vec<Song>
}

#[allow(non_snake_case)]
#[derive(Debug, Deserialize, Clone)]
pub struct Song {
    pub _id: String,
    pub createdAt: String,
    pub updatedAt: String,
    pub track: SongTrack,
    #[serde(skip_deserializing)]
    pub user: Option<String>,
}

impl PartialEq for Song {
    fn eq(&self, other: &Self) -> bool {
        self._id == other._id
    }
}

#[allow(non_snake_case)]
#[derive(Debug, Deserialize, Clone)]
pub struct SongTrack {
    pub providerId: String,
    pub provider: String,
    pub duration: u32,
    pub title: String,
    pub artist: String,
    pub url: String,
    pub _position: Option<u32>
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct NightBotResp {
    pub access_token: String,
    pub token_type: String,
    pub expires_in: u32,
    pub refresh_token: String,
    pub scope: String
}
