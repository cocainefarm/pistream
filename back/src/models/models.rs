use std::cmp::Ordering;
use serde::{Serialize, Deserialize};
use chrono::{DateTime, Local, Utc, TimeZone};
use serde::Deserializer;
use serde::de::{self};
use std::fmt;

#[derive(Debug, Serialize, Deserialize)]
pub struct Resp<T> {
    pub r#type: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub items: Option<Vec<T>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub item: Option<T>
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Token {
    pub token: String
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Streamlabs {
    pub data: Data
}

impl Streamlabs {
    pub fn get_all_messages(&self) -> Vec<Message> {
        let mut vec = Vec::new();
        let mut data = self.data.clone();
        vec.append(&mut data.donations);
        vec.append(&mut data.hosts);
        vec.append(&mut data.subscriptions);
        vec.append(&mut data.follows);
        vec.append(&mut data.bits);
        vec.append(&mut data.raids);
        vec
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Data {
    pub donations: Vec<Message>,
    pub hosts: Vec<Message>,
    pub subscriptions: Vec<Message>,
    pub follows: Vec<Message>,
    pub bits: Vec<Message>,
    pub raids: Vec<Message>
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Event {
    pub r#type: String,
    pub message: Vec<Message>,
    pub fro: Option<String>,
    pub event_id: Option<String>
}

struct Convert;

impl<'de> de::Visitor<'de> for Convert {
    type Value = Option<String>;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("an integer or a string")
    }

    fn visit_u64<E>(self, v: u64) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(Some(v.to_string()))
    }

    fn visit_string<E>(self, v: String) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(Some(v))
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(Some(v.to_string()))
    }

    fn visit_none<E>(self) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(None)
    }

    fn visit_unit<E>(self) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(None)
    }
}

struct Month;

impl<'de> de::Visitor<'de> for Month {
    type Value = Option<u16>;
    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("an integer or a string")
    }

    fn visit_u64<E>(self, v: u64) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(Some(v as u16))
    }

    fn visit_string<E>(self, v: String) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(Some(v.parse::<u16>().unwrap_or(0)))
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(Some(v.parse::<u16>().unwrap_or(0)))
    }

    fn visit_none<E>(self) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(None)
    }

    fn visit_unit<E>(self) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(None)
    }
}

fn month<'de, D>(deserializer: D) -> Result<Option<u16>, D::Error>
where D: Deserializer<'de>{
    deserializer.deserialize_any(Month)
}

fn convert<'de, D>(deserializer: D) -> Result<Option<String>, D::Error>
where D: Deserializer<'de>{
    deserializer.deserialize_any(Convert)
}

#[derive(Debug, Serialize, Deserialize, Eq, Clone)]
pub struct Message {
    #[serde(default, deserialize_with = "convert")]
    pub id: Option<String>,
    pub _id: Option<String>,
    pub name: Option<String>,
    pub formatted_amount: Option<String>,
    pub message: Option<String>,
    #[serde(default, deserialize_with = "month")]
    pub months: Option<u16>,
    pub sub_plan: Option<String>,
    pub sub_type: Option<String>,
    pub gifter: Option<String>,
    pub created_at: Option<String>,
    pub viewers: Option<u16>,
    pub r#type: Option<String>,
    #[serde(default, deserialize_with = "convert")]
    pub amount: Option<String>,
    pub raiders: Option<u16>,
    pub priority: Option<u16>
}


impl Message {
    pub fn empty() -> Message {
        Message {
            id: None,
            _id: None,
            name: None,
            formatted_amount: None,
            message: None,
            months: None,
            sub_plan: None,
            sub_type: None,
            gifter: None,
            created_at: None,
            viewers: None,
            r#type: None,
            amount: None,
            raiders: None,
            priority: None,
        }
    }

    pub fn add_time(&mut self) {
        let time = Local::now();
        self.created_at = Some(time.to_rfc3339());
    }

    pub fn convert_time(&mut self) {
        match &self.created_at {
            Some(t) => {
                let time = Utc.datetime_from_str(&t, "%Y-%m-%d %H:%M:%S").unwrap();
                let time = Local.from_utc_datetime(&time.naive_local());
                self.created_at = Some(time.to_rfc3339());
            },
            None => self.add_time()
        }
    }
}

impl Ord for Message {
    fn cmp(&self, other: &Self) -> Ordering {
        self.created_at.cmp(&other.created_at)
    }
}

impl PartialOrd for Message {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for Message {
    fn eq(&self, other: &Self) -> bool {
        let date = self.created_at.clone().unwrap();
        let self_date = DateTime::parse_from_rfc2822(&date).unwrap();
        let date = other.created_at.clone().unwrap();
        let other_date = DateTime::parse_from_rfc2822(&date).unwrap();
        self_date.timestamp_millis() == other_date.timestamp_millis()
    }
}

#[derive(Debug, Clone, Copy, Serialize)]
pub struct LiveStream {
    pub live: bool,
    pub connection: bool,
    pub bitrate: f32,
    pub stop_stream: bool,
    pub reconnect: bool
}

impl LiveStream {
    pub fn new() -> LiveStream {
        LiveStream {
            live: false,
            connection: false,
            bitrate: 0.0,
            stop_stream: false,
            reconnect: true
        }
    }
}

impl PartialEq for LiveStream {
    fn eq(&self, other: &Self) -> bool {
        self.live == other.live && self.connection == other.connection && self.bitrate == other.bitrate
    }
}

