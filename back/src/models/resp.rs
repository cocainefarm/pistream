use serde::Serialize;

#[derive(Debug, Serialize)]
pub struct Resp<T> {
    #[serde( rename = "type" )]
    t: String,
    data: T
}

impl<T> Resp<T> {
    pub fn new(t: String, data: T) -> Resp<T> {
        Resp {
            t,
            data
        }
    }

    pub fn new_stream(data: T) -> Resp<T> {
        Resp {
            t: "stream".to_string(),
            data
        }
    }

    pub fn new_error(data: T) -> Resp<T> {
        Resp {
            t: "error".to_string(),
            data
        }
    }
}
