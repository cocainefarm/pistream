use serde::{Deserialize, Serialize};
use std::{fs, fs::File};
use std::io::Write;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Config {
    pub res: String,
    pub fps: String,
    pub bitrate: String,
    pub stream_url: String,
    pub encoder: String,
    //alerts
    pub streamlabs_token: String,
    pub streamlabs_event_id: String,
    //pc backend
    pub pc_ip: String,
    pub pc_port: u16,
    //sound request
    pub nightbot_client_id: String,
    pub nightbot_client_secret: String,
    pub nightbot_refresh_token: String,
    //twitch oauth
    pub twitch_profile_id: String,
    pub twitch_cliend_id: String,
    pub twitch_cliend_secret: String,
    pub twitch_refresh_token: String
}

impl Config {
    pub fn load() -> Config {
        let base = xdg::BaseDirectories::with_prefix("pistream").unwrap();
        let path = base.place_config_file("config.json").unwrap();
        let config_str = match fs::read_to_string(&path) {
            Ok(s) => s,
            Err(_) => {
                Config::create();
                panic!("configure your configfile at {}", path.to_str().unwrap())
            }
        };
        match serde_json::from_str::<Config>(&config_str) {
            Ok(j) => {
                j.check_if_empty();
                j
            },
            Err(_) => panic!("config file empty or brocken")
        }
    }

    pub fn check_if_empty(&self) {
        if self.res == "" || self.res.contains(" ") {
            panic!("invalid res in config: '{}'", self.res)
        }
        if self.fps == "" || self.res.contains(" ") {
            panic!("invalid fps in config: '{}'", self.fps)
        }
        if self.bitrate == "" || self.bitrate.contains(" ") {
            panic!("invalid bitrate in config: '{}'", self.bitrate)
        }
        if self.stream_url == "" || self.stream_url.contains(" ") {
            panic!("invalid stream_url in config: '{}'", self.bitrate)
        }
        if self.encoder == "" || self.encoder.contains(" ") {
            panic!("invalid encoder in config: '{}'", self.bitrate)
        }
        if self.streamlabs_token.contains(" ") {
            panic!("invalid streamlabs_token in config: '{}'", self.streamlabs_token)
        }
        if self.streamlabs_event_id.contains(" ") {
            panic!("invalid streamlabs_event_id in config: '{}'", self.streamlabs_event_id)
        }
        if self.pc_ip == "" || self.pc_ip.contains(" ") || !self.pc_ip.contains(".") {
            panic!("invalid pc_ip in config: '{}'", self.pc_ip)
        }
        if self.twitch_profile_id.contains(" ") {
            panic!("invalid twitch_profile_id in config: '{}'", self.twitch_profile_id)
        }
        if self.twitch_cliend_id.contains(" ") {
            panic!("invalid twitch_cliend_id in config: '{}'", self.twitch_cliend_id)
        }
        if self.twitch_cliend_secret.contains(" ") {
            panic!("invalid twitch_cliend_secret in config: '{}'", self.twitch_cliend_secret)
        }
    }

    pub fn create() {
        let base = xdg::BaseDirectories::with_prefix("pistream").unwrap();
        let path = base.place_config_file("config.json").unwrap();
        let mut file = File::create(&path).unwrap();
        let empty = "".to_string();
        let config = Config {
            res: "1280x720".to_string(),
            fps: "30".to_string(),
            bitrate: "4500k".to_string(),
            stream_url: "srt://localhost:8000/".to_string(),
            encoder: "hevc_nvmpi".to_string(),
            streamlabs_token: empty.clone(),
            streamlabs_event_id: empty.clone(),
            pc_ip: empty.clone(),
            pc_port: 8000,
            nightbot_client_id: empty.clone(),
            nightbot_client_secret: empty.clone(),
            nightbot_refresh_token: empty.clone(),
            twitch_profile_id: empty.clone(),
            twitch_cliend_id: empty.clone(),
            twitch_cliend_secret: empty.clone(),
            twitch_refresh_token: empty
        };
        let config = serde_json::to_string_pretty(&config).unwrap();
        let _ = file.write_all(config.as_bytes()).unwrap();
    }

    pub fn save(&self) {
        let base = xdg::BaseDirectories::with_prefix("pistream").unwrap();
        let path = base.place_config_file("config.json").unwrap();
        let mut file = File::create(&path).unwrap();
        let config = serde_json::to_string_pretty(&self).unwrap();
        let _ = file.write_all(config.as_bytes()).unwrap();
    }
}
