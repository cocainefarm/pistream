use serde::{Serialize, Deserialize};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ConfigReward {
    pub sound_path: String,
    pub name: String,
    pub is_enabled: bool,
    pub is_tts: bool,
    pub cost: u32,
    pub twitch_reward: Option<Reward>
}

impl ConfigReward {
    pub fn new(name: String, sound_path: String) -> Self {
        ConfigReward {
            sound_path,
            name,
            is_enabled: false,
            is_tts: false,
            cost: 100,
            twitch_reward: None
        }
    }

    pub fn new_tts() -> Self {
        ConfigReward {
            sound_path: "tts.ogg".to_string(),
            name: "TTS".to_string(),
            is_enabled: false,
            is_tts: false,
            cost: 100,
            twitch_reward: None
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct TwitchResp<T> {
    pub data: Vec<T>
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Reward {
    pub id: String,
    pub is_enabled: bool,
    pub cost: u32,
    pub title: String,
    pub is_user_input_required: bool,
    pub max_per_stream_setting: StreamSetting,
    pub max_per_user_per_stream_setting: UserSetting,
    pub global_cooldown_setting: CooldownSetting,
    pub is_paused: bool,
    pub should_redemptions_skip_request_queue: bool
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct UserSetting {
    pub is_enabled: bool,
    pub max_per_user_per_stream: u32
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct StreamSetting {
    pub is_enabled: bool,
    pub max_per_stream: u32
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct CooldownSetting {
    pub is_enabled: bool,
    pub global_cooldown_seconds: u32
}

#[derive(Clone, Debug, Deserialize)]
pub struct RefreshToken {
    pub access_token: String,
    pub refresh_token: String
}
