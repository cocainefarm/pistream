use std::sync::{mpsc, Arc, Mutex};
use std::path::Path;

use crate::twitch::pubsub;
use crate::player::player::{AlertMsg, tts};

pub async fn soundboard_handler(
    sender: Arc<Mutex<mpsc::Sender<AlertMsg>>>,
    resiv: mpsc::Receiver<pubsub::PubsubRedemtion>
) {
    let path = Path::new("/home/d3fus/music/soundboard");
    for msg in resiv {
        if msg.reward.title.contains("TTS") {
            let text = msg.user_input.unwrap_or("empty tts".to_string());
            let tts_path = path.join(format!("{}_tts.ogg", msg.id));
            tts(&text, tts_path.clone()).await;
            let s = sender.lock().unwrap();
            let _ = s.send(AlertMsg::Sound(tts_path, None));
            drop(s);
        } else {
            println!("{:?}", msg.reward.title);
            let sound = msg.reward.title.split("SR: ").collect::<Vec<&str>>()[1];
            println!("{:?}", sound);
            let s = sender.lock().unwrap();
            let _ = s.send(AlertMsg::Sound(path.join(&sound), None));
            drop(s);
        }
    }
}
