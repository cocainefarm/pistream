use std::io::BufReader;
use rodio;
use std::fs;
use std::{thread, time};
use std::process::{Command, Stdio};
use std::sync::{Arc, Mutex};
use std::sync::mpsc;
use serde::Deserialize;
use std::path::PathBuf;
use rand::thread_rng;
use rand::Rng;
use rand::seq::SliceRandom;

use crate::nightbot::api;

#[derive(Debug)]
pub enum Message {
    Play,
    Stop,
    Skip,
    PlayAlert,
    StopAlert,
    AddSong(String)
}

pub async fn start_player(token: String, resiv: mpsc::Receiver<Message>) {

    //create new sink to play music
    let (_stream, handle) = rodio::OutputStream::try_default().unwrap();
    let mut sink = rodio::Sink::try_new(&handle).unwrap();
    sink.pause();

    //wait to connect to bluetooth speaker
    loop {
        let out = Command::new("ls")
            .arg("/dev/input/event3")
            .stdout(Stdio::null())
            .stdout(Stdio::null())
            .stderr(Stdio::null())
            .status()
            .unwrap();
        if let Some(code) = out.code() {
            if code != 2 {
                break
            }
            let _ = Command::new("$HOME/scripts/connect.sh").output();
        }
        let sec = time::Duration::from_secs(15);
        thread::sleep(sec);
    }

    //prevent blutooth disconnect
    let _ = thread::spawn(|| play_empty());

    let (_stream2, handle2) = rodio::OutputStream::try_default().unwrap();
    let mut autoplay = Autoplay::new(&handle2);

    let mut playlist: Vec<String> = Vec::new();
    for msg in resiv {
        println!("{:?}", msg);
        match msg {
            Message::Play => {
                println!("empty: {}", sink.empty());
                if sink.empty() {
                    autoplay.play();
                } else {
                    sink.set_volume(1.0);
                    sink.play();
                }
            },
            Message::Stop => {
                sink.pause();
                autoplay.pause();
            },
            Message::Skip => {
                if !sink.empty() {
                    let x = sink.len();
                    let playlist2 = playlist.split_off(playlist.len() - (x -1));
                    for p in &playlist {
                        fs::remove_file(p).unwrap();
                    }
                    playlist = playlist2;
                    sink.stop();
                    sink = rodio::Sink::try_new(&handle).unwrap();
                    for p in &playlist {
                        let file = open_file(p);
                        sink.append(file);
                    }
                    sink.play();
                } else {
                    autoplay.skip(&handle2);
                }
            },
            Message::AddSong(path) => {
                autoplay.pause();
                playlist.push(path.clone());
                let file = open_file(&path);
                let p = sink.is_paused();
                sink.append(file);
                if p {
                    sink.pause();
                }
                let id: &str = path
                    .split('_')
                    .collect::<Vec<&str>>()[1]
                    .split('.')
                    .collect::<Vec<&str>>()[0];
                api::del_item(&token, id).await;
            },
            Message::PlayAlert => {
                sink.set_volume(0.3);
                autoplay.volume(0.3);
            },
            Message::StopAlert => {
                sink.set_volume(1.0);
                autoplay.volume(1.0);
            },
            _ => {}
        }
    }
}

struct Autoplay {
    sink: rodio::Sink,
    playlist: Vec<PathBuf>
}

impl Autoplay {
    pub fn new(handle: &rodio::OutputStreamHandle) -> Self {
        let mut sink = rodio::Sink::try_new(&handle).unwrap();
        let dir = fs::read_dir("/home/d3fus/music/autoplay").unwrap();

        let mut dirs = dir
            .map(|d| d.map(|e| e.file_name()))
            .collect::<Result<Vec<_>, std::io::Error>>().unwrap();

        let mut rng = rand::thread_rng();
        let r = rng.gen_range(0.. dirs.len());
        let path = format!("/home/d3fus/music/autoplay/{}", dirs[r].to_str().unwrap());
        let files = fs::read_dir(&path).unwrap();
        let mut playlist = files
            .map(|f| f.map(|e| e.path()))
            .collect::<Result<Vec<_>, std::io::Error>>().unwrap();
        playlist.shuffle(&mut rng);
        sink.pause();
        for m in &playlist {
            let path = m.to_str().unwrap();
            let file = open_file(path);
            sink.append(file);
        }
        sink.set_volume(1.0);
        Self {
            sink,
            playlist
        }
    }

    pub fn play(&mut self) {
        self.sink.set_volume(1.0);
        self.sink.play();
    }

    pub fn pause(&mut self) {
        self.sink.pause();
    }

    pub fn volume(&mut self, volume: f32) {
        self.sink.set_volume(volume);
    }

    pub fn skip(&mut self, handle: &rodio::OutputStreamHandle) {
        let x = self.sink.len();
        let playlist2 = self.playlist.split_off(self.playlist.len() - (x -1));
        self.playlist = playlist2;
        self.sink.stop();
        self.sink = rodio::Sink::try_new(&handle).unwrap();
        for p in &self.playlist {
            let file = open_file(p.to_str().unwrap());
            self.sink.append(file);
        }
        self.sink.play();
    }
}

pub fn open_file(path: &str) -> rodio::Decoder<BufReader<std::fs::File>> {
    let file = std::fs::File::open(path).unwrap();
    rodio::Decoder::new(BufReader::new(file)).unwrap()
}


#[derive(Deserialize)]
struct StramlabsTTS {
    success: bool,
    speak_url: String
}

use std::io::copy;
use std::fs::File;

pub async fn tts(msg: &str, path: PathBuf) -> Result<(), String> {
    let client = reqwest::Client::new();
    let data = [
        ("voice", "Brian"),
        ("text", msg)
    ];
    let req = client.post("https://streamlabs.com/polly/speak")
        .form(&data)
        .send();
    let j: StramlabsTTS = req.await.unwrap().json().await.unwrap();
    println!("{:?}", j.success);
    let mut req = reqwest::blocking::get(&j.speak_url).unwrap();
    //let mut file = File::create(format!("/home/d3fus/music/{}/tts.ogg", path)).unwrap();
    let mut file = File::create(&path).unwrap();
    //let text = req.text().await.unwrap();
    copy(&mut req, &mut file).unwrap();
    Ok(())
}

pub enum AlertMsg {
    Sound(PathBuf, Option<PathBuf>)
}

pub fn alerts_player(resiv: mpsc::Receiver<AlertMsg>, sender: Arc<Mutex<mpsc::Sender<Message>>>) {
    let (_stream, handle) = rodio::OutputStream::try_default().unwrap();
    let sink = rodio::Sink::try_new(&handle).unwrap();
    sink.set_volume(1.0);
    for msg in resiv {
        sink.pause();
        let AlertMsg::Sound(path, tts) = msg;
        let file = std::fs::File::open(path).unwrap();
        sink.append(rodio::Decoder::new(BufReader::new(file)).unwrap());
        if let Some(path) = tts.clone() {
            let file = std::fs::File::open(path).unwrap();
            sink.append(rodio::Decoder::new(BufReader::new(file)).unwrap());
        }
        let s = sender.lock().unwrap();
        let _ = s.send(Message::PlayAlert);
        drop(s);
        sink.play();
        sink.sleep_until_end();
        let s = sender.lock().unwrap();
        let _ = s.send(Message::StopAlert);
        drop(s);
        if let Some(path) = tts {
            let _ = fs::remove_file(&path);
        }
    }
}

pub fn play_empty() {
    loop {
        let (_stream, handle) = match rodio::OutputStream::try_default() {
            Ok(s) => s,
            Err(_) => {
                let min = time::Duration::from_secs(60 * 8 );
                thread::sleep(min);
                panic!("asdfasdf");
            }
        };
        match rodio::Sink::try_new(&handle) {
            Ok(sink) => {
                let file = std::fs::File::open("/home/d3fus/music/empty.mp3").unwrap();
                sink.append(match rodio::Decoder::new(BufReader::new(file)) {
                    Ok(o) => o,
                    Err(_) => panic!("asd")
                });
                sink.set_volume(0.01);

                sink.play();
                println!("empty");
                let sec = time::Duration::from_secs(5);
                thread::sleep(sec);
                sink.stop();
                let min = time::Duration::from_secs(60);
                thread::sleep(min);
            },
            Err(_) => {
                let min = time::Duration::from_secs(60);
                thread::sleep(min);
            }
        };

    }
}
