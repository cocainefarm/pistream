use std::sync::{Arc, Mutex, mpsc};
use std::process::{Command, Stdio};
use std::{thread, time};
use std::fs;
use std::path::Path;
use std::io::BufReader;
use std::io::prelude::*;
use std::os::unix::net::UnixListener;
use rand::Rng;

use crate::player::player::{AlertMsg, tts};
use crate::models::models::Event;
use crate::api::ws::WsUpdate;

pub async fn alerts_handler(
    sender: Arc<Mutex<mpsc::Sender<AlertMsg>>>,
    ws_sender: Arc<Mutex<mpsc::Sender<WsUpdate>>>
) {
    loop {
        let out = Command::new("ls")
            .arg("/dev/input/event3")
            .stdout(Stdio::null())
            .stdout(Stdio::null())
            .stderr(Stdio::null())
            .status()
            .unwrap();
        if let Some(code) = out.code() {
            if code != 2 {
                break
            }
            let _ = Command::new("$HOME/scripts/connect.sh").output();
        }
        let sec = time::Duration::from_secs(15);
        thread::sleep(sec);
    }
    let home = std::env::var("HOME").unwrap();
    if Path::new("/tmp/socketio.sock").exists() {
        fs::remove_file("/tmp/socketio.sock").unwrap();
    }
    let _ = Command::new(Path::new(&home).join("sock.py").to_str().unwrap())
        .spawn()
        .unwrap();
    let listener = UnixListener::bind("/tmp/socketio.sock").unwrap();
    //NOTE path to alerts
    let path = Path::new("/home/d3fus/music/streamlabs");
    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                let stream = BufReader::new(stream);
                for line in stream.lines() {
                    let mut play_tts = false;
                    let l: &str = &line.unwrap();
                    println!("{:?}", l);
                    if !l.contains("alertPlaying") && !l.contains("subMysteryGift") {
                        let test_event = serde_json::from_str::<Event>(l.clone());
                        println!("{:?}", test_event);
                        if let Ok(mut event) = serde_json::from_str::<Event>(l) {
                            event.message[0].convert_time();
                            event.message[0].r#type = Some(event.r#type.clone());
                            let s = ws_sender.lock().unwrap();
                            s.send(
                                WsUpdate::Event(
                                    serde_json::to_string(&event.message[0].clone()).unwrap()
                                )
                            );
                            drop(s);
                            let dir = Path::new(&home).join("music/streamlabs");

                            //NOTE get the id from alert or gen random one
                            let mut rng = rand::thread_rng();
                            let id = event.event_id.clone().unwrap_or(rng.gen::<u32>().to_string());
                            //NOTE create tts path
                            let tts_path = path.join(format!("{}_tts.ogg", id));

                            //TODO add text to speech
                            let path = match event.r#type.as_str(){
                                "follow" => {
                                    Some(dir.join("follow.mp3"))
                                },
                                "subscription" => {
                                    if let Some(msg) = &event.message[0].message {
                                        let _ = tts(&msg, tts_path.clone()).await;
                                        play_tts = true;
                                    }
                                    Some(dir.join("sub.mp3"))
                                },
                                "resub" => {
                                    if let Some(msg) = &event.message[0].message {
                                        let _ = tts(&msg, tts_path.clone()).await;
                                        play_tts = true;
                                    }
                                    Some(dir.join("sub.mp3"))
                                },
                                "bits" => {
                                    if let Some(msg) = &event.message[0].message {
                                        let _ = tts(&msg, tts_path.clone()).await;
                                        play_tts = true;
                                    }
                                    Some(dir.join("bits.ogg"))
                                },
                                _ => Some(dir.join("default.ogg"))
                            };
                            if let Some(p) = path {
                                let mut t = None;
                                if play_tts {
                                    t = Some(tts_path);
                                }
                                let s = sender.lock().unwrap();
                                let _ = s.send(AlertMsg::Sound(p, t));
                                drop(s);
                            }
                            println!("{:?}", event);
                        }
                    }
                }
            }
            Err(err) => {
                println!("Error: {}", err);
                break;
            }
        }
    }
}
