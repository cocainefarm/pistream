use std::process::{Command, Stdio};
use std::sync::{Arc, Mutex, mpsc::Sender, mpsc::Receiver};
use std::{thread, time};

use crate::models::config::Config;
use crate::api::{ws::WsUpdate, stream::Stream};

pub fn ffmpeg_stream(
    sender: Arc<Mutex<Sender<WsUpdate>>>,
    resiv: Receiver<Stream>,
    config: &Config
) {
    let mut stream = false;
    loop {
        if !stream {
            for msg in &resiv {
                if let Stream::Start = msg {
                    stream = true;
                    break;
                }
            }
        }
        //kill this thread if not started
        if !stream {
            break
        }
        let mut out = Command::new("ffmpeg")
            .arg("-thread_queue_size")
            .arg("1024")
            .arg("-f").arg("alsa").arg("-ac")
            .arg("2").arg("-i").arg("hw:2,0")
            .arg("-f").arg("v4l2")
            .arg("-input_format")
            .arg("yuyv422")
            .arg("-i")
            .arg("/dev/video0")
            .arg("-c:v")
            //.arg("h264_nvmpi")
            .arg(&config.encoder)
            .arg("-s")
            .arg(&config.res)
            .arg("-r")
            .arg(&config.fps)
            .arg("-b:v")
            .arg(&config.bitrate)
            .arg("-ar")
            .arg("44100")
            .arg("-f")
            .arg("mpegts")
            .arg("-transtype")
            .arg("live")
            .arg("-latency")
            .arg("100000")
            .arg(&config.stream_url)
            .arg("-progress")
            .arg("-")
            .arg("-nostats")
            .stdout(Stdio::piped())
            .spawn()
            .expect("error starting stream");
        use std::io::{BufReader, BufRead};
        {
            let stdout = out.stdout.as_mut().unwrap();
            let stdout_reader = BufReader::new(stdout);
            let stdout_lines = stdout_reader.lines();

            for line in stdout_lines {
                if let Ok(line) = line {
                    if line.contains("bitrate") {
                        //Get current bitrate from sdiout
                        let bit: Vec<&str>;
                        if line.contains("= ") {
                            bit = line.split("= ").collect();
                        } else {
                            bit = line.split("=").collect();
                        }
                        let bit: Vec<&str> = bit[1].split("kbits").collect();
                        let bit: f32 = if bit[0] != "N/A" {
                            match bit[0].parse() {
                                Ok(b) => b,
                                Err(_) => 0.0
                            }
                        } else {
                            0.0
                        };

                        //Update current bitrate

                        let s = sender.lock().unwrap();
                        if bit < 2000.0 {
                            let _ = s.send(WsUpdate::Online(false));
                        } else {
                            let _ = s.send(WsUpdate::Online(true));
                        }
                        let _ = s.send(WsUpdate::Bitrate(bit));
                        drop(s);
                    }
                }

                //Stop stream if signal is stop
                if let Ok(Stream::Stop) = resiv.try_recv() {
                    stream = false;
                    let s = sender.lock().unwrap();
                    let _ = s.send(WsUpdate::Bitrate(0.0));
                    let _ = s.send(WsUpdate::Online(false));
                    drop(s);
                    break
                }
            }

        }
        match out.kill() {
            Ok(o) => {
                println!("{:?}", o);
            },
            Err(e) => {
                println!("{:?}", e);
            }
        };

        let s = sender.lock().unwrap();
        let _ = s.send(WsUpdate::Bitrate(0.0));
        let _ = s.send(WsUpdate::Online(false));
        drop(s);
        if let Ok(Stream::Stop) = resiv.try_recv() {
            stream = false;
        }

        let sec = time::Duration::from_secs(2);
        thread::sleep(sec);
    }
}
