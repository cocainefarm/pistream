use std::fs;
use std::sync::{Arc, Mutex, mpsc};
use std::{thread, time};

use crate::api::ws::WsUpdate;

pub fn start_temp(sender: Arc<Mutex<mpsc::Sender<WsUpdate>>>) {
    let mut last = 0.0;
    loop {
        let tmp_temp = fs::read_to_string("/sys/class/thermal/thermal_zone0/temp")
            .expect("error reding temp");
        let tmp_temp: String = tmp_temp.split("\n").collect();
        let mut temp = tmp_temp.parse().unwrap();
        temp = temp / 1000.0;

        if temp != last {
            last = temp;
            let send = sender.lock().unwrap();
            let _ = send.send(WsUpdate::TempUpdate(temp));
            drop(send);
        }

        let sec = time::Duration::from_secs(1);
        thread::sleep(sec);
    }
}
