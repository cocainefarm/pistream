#[macro_use] extern crate rocket;
use std::process::{Command, Stdio};
use rocket::config::{Config as RocketConfig, TlsConfig};
use std::net::IpAddr;
use std::str::FromStr;
use std::thread;
use std::sync::{Arc, Mutex, mpsc};
use rocket::tokio::runtime::Runtime;
use rocket_contrib::serve::StaticFiles;

mod models;
mod player;
mod twitch;
mod api;
mod misc;
mod nightbot;

use crate::models::{
    status::Status,
    config::Config
};
use crate::nightbot::{api::get_tokens, downloader::Downloader};

////wireguard losing connection if not
fn start_ping(config: Config) {
    let _ = Command::new("ping")
        .args(&["-i", "10", &config.pc_ip])
        .stdout(Stdio::null())
        .spawn()
        .expect("error pining");
}

#[rocket::main]
async fn main() {
    let mut config = Config::load();
    let status = Status::new();

    //update tokens in configfile
    let mut nightbot_token = "".to_string();
    if config.nightbot_client_secret != "" {
        nightbot_token = get_tokens(&mut config).await;
    }

    let mut twitch_token = "".to_string();
    if config.twitch_cliend_secret != "" {
        twitch_token = match twitch::rewards::renew_token(&mut config).await {
            Ok(token) => token,
            Err(_) => "".to_string()
        };
    }

    //starts pining the host
    let config_clone = config.clone();
    thread::spawn(|| start_ping(config_clone));

    //create main msg channel
    let (send, recv) = mpsc::channel();

    // Start Websocket for Webshit
    thread::spawn(|| api::ws::start_server(recv));

    let send = Arc::new(Mutex::new(send));

    //Send temp over channel to websocket
    let send_clone = send.clone();
    thread::spawn(|| misc::temp::start_temp(send_clone));

    //create channel for music player
    let (player_send, palyer_recv) = mpsc::channel();
    let player_send = Arc::new(Mutex::new(player_send));
    //start main music player
    if config.nightbot_client_id != "" {
        let nightbot_token_clone = nightbot_token.clone();
        thread::spawn(move || {
            Runtime::new()
                .expect("Failed to create Tokio runtime")
                .block_on(player::player::start_player(nightbot_token_clone, palyer_recv));
        });

        //start downloader
        let player_send_clone = player_send.clone();
        let send_clone = send.clone();
        thread::spawn(move || {
            let mut down = Downloader::new(nightbot_token.clone());
            Runtime::new()
                .expect("Failed to create Tokio runtime")
                .block_on(down.start(player_send_clone, send_clone));
        });
    }

    //wrap the sender

    let (alert_send, alert_recv) = mpsc::channel();
    let alert_send = Arc::new(Mutex::new(alert_send));
    //start alerts and soundboard
    if &config.streamlabs_token != "" {
        let player_send_clone = player_send.clone();
        thread::spawn(|| player::player::alerts_player(alert_recv, player_send_clone));
        let alert_send_clone = alert_send.clone();
        let send_clone = send.clone();
        thread::spawn(|| {
            Runtime::new()
                .expect("Failed to create Tokio runtime")
                .block_on(player::alerts::alerts_handler(alert_send_clone, send_clone));
        });
    }

    //start pubsub for soundboard
    if &config.twitch_cliend_secret != "" {
        let (sound_send, sound_recv) = mpsc::channel();
        let alert_send_clone = alert_send.clone();
        thread::spawn(|| twitch::pubsub::start_pubsub(sound_send));
        thread::spawn(|| {
            Runtime::new()
                .expect("Failed to create Tokio runtime")
                .block_on(player::soundboard::soundboard_handler(alert_send_clone, sound_recv));
        });
    }

    //create own channel for starting ffmpeg and stoping it.
    let (ffmpeg_send, ffmpeg_recv) = mpsc::channel::<api::stream::Stream>();
    let ffmpeg_send = Arc::new(Mutex::new(ffmpeg_send));
    //start ffmpeg in the background and wait for start signal
    let send_clone = send.clone();
    let config_clone = config.clone();
    thread::spawn(move || misc::ffmpeg::ffmpeg_stream(send_clone, ffmpeg_recv, &config_clone));


    //start rewardscanning if token valid

    let rewards = twitch::rewards::Rewards::new(twitch_token.clone(), config.clone());
    let rewards = Arc::new(Mutex::new(rewards));

    let (reward_sedner, reward_recv) = mpsc::channel();
    let reward_sender = Arc::new(Mutex::new(reward_sedner));
    let config_clone = config.clone();
    let twitch_token_clone = twitch_token.clone();
    let rewards_clone = rewards.clone();
    thread::spawn(|| {
        Runtime::new()
            .expect("Failed to create Tokio runtime")
            .block_on(twitch::rewards::start_reards(rewards_clone, twitch_token_clone, config_clone, reward_recv));
    });

    let all = IpAddr::from_str("0.0.0.0").unwrap();
    let mut rocket_conf = RocketConfig::default();
    rocket_conf.address = all;
    rocket_conf.port = 3030;
    if config.twitch_cliend_secret != "" {
        rocket_conf.tls = Some(TlsConfig::from_paths("./private/cert.pem", "./private/key.pem"));
    }

    let twitch_token = Arc::new(Mutex::new(twitch_token));

    let route = routes![
        api::web::get_index,
        api::web::get_bulma,
        api::web::get_wasm_js,
        api::web::get_wasm_bg,
        api::web::get_nerd_fonts,
        api::web::get_nerd_fonts_font,
        api::stream::start_stream,
        api::stream::stop_stream,
        api::stream::start_obs,
        api::stream::stop_obs,
        api::status::get_config,
        api::status::get_last_events,
        api::music::start_music,
        api::music::stop_music,
        api::music::skip_music,
        api::twitch::token_valide,
        api::twitch::update_token,
        api::twitch::get_rewards,
        api::twitch::update_reward
    ];
    rocket::custom(rocket_conf)
        .mount("/", route)
        .manage(ffmpeg_send)
        .manage(config)
        .manage(twitch_token)
        .manage(reward_sender)
        .manage(rewards)
        .manage(send)
        .manage(player_send)
        .launch()
        .await;
}
